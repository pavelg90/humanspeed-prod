// JavaScript Document
jQuery(document).ready(function($) {
	menuButton		= $('.nav__mobile__btn');
	
	$(menuButton).on('click', function() {
		if( $('.nav__primary').attr('data-menu-active') == 'false' ) {
			$('.nav__primary').attr('data-menu-active', 'true');
			$('.nav__mobile__btn').attr('data-show-menu', 'true');	
		}
		else {
			$('.nav__primary').attr('data-menu-active', 'false');
			$('.nav__mobile__btn').attr('data-show-menu', 'false');
		}
	});	
});