<?php
/**
 * Magento Extension by TRIC Solutions
 *
 * @copyright  Copyright (c) 2012 TRIC Solutions (http://www.tric.dk)
 * @license    http://www.tric.dk/TRIC-LICENSE-COMMUNITY.txt
 * @store       http://store.tric.dk
 */

class TRIC_GLS_Model_Adminhtml_System_Config_Source_Cachettl
{
    public function toOptionArray()
    {
	    $array[] = array('value'=>1*24*60*60, 'label'=>Mage::helper('gls')->__('1 Døgn'));
	    $array[] = array('value'=>2*24*60*60, 'label'=>Mage::helper('gls')->__('2 Døgn'));
  	    $array[] = array('value'=>3*24*60*60, 'label'=>Mage::helper('gls')->__('3 Døgn'));
	    $array[] = array('value'=>7*24*60*60, 'label'=>Mage::helper('gls')->__('1 Uge')); 
	    $array[] = array('value'=>14*24*60*60, 'label'=>Mage::helper('gls')->__('2 Uger')); 
	    $array[] = array('value'=>30*24*60*60, 'label'=>Mage::helper('gls')->__('1 Måned')); 
        return $array;
    }
}
