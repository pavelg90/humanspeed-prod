<?php
/**
 * Magento Extension by TRIC Solutions
 *
 * @copyright  Copyright (c) 2013 TRIC Solutions (http://www.tric.dk)
 * @license    http://www.tric.dk/TRIC-LICENSE-COMMUNITY.txt
 * @store       http://store.tric.dk
 */

class TRIC_GLS_Helper_Droppoints extends Mage_Core_Helper_Abstract
{
	public function getDroppointsIds()
	{
		$resource = Mage::getSingleton('core/resource');
		$read = $resource->getConnection('core_read');
		$tablename = $resource->getTableName('shipping_gls');
		
		$query = "SELECT pk FROM $tablename WHERE method_code LIKE '%gls_pakkeshop%'";
		$results = $read->fetchAll($query);
		$ids = array();
		foreach($results as $result)
		{
			$ids[] = $result['pk'];
		}
		return $ids;
	}	
	
	public function getFlexIds()
	{
		$resource = Mage::getSingleton('core/resource');
		$read = $resource->getConnection('core_read');
		$tablename = $resource->getTableName('shipping_gls');
		
		$query = "SELECT pk FROM $tablename WHERE method_code LIKE '%gls_privat%'";
		$results = $read->fetchAll($query);
		$ids = array();
		foreach($results as $result)
		{
			$ids[] = $result['pk'];
		}
		return $ids;
	}
	
	public function getDroppoint($droppoint)
	{
		$droppoint = explode("-",$droppoint);
		$handikapvenlig = false;
		if(isset($droppoint[1]))
		{
			$droppoint = $droppoint[0];
			$handikapvenlig = true;			
		}
		else
		{
			$droppoint = $droppoint[0];
		}
		
		return $droppoint;
	}	
}