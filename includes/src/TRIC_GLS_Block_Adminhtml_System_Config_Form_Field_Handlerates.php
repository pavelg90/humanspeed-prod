<?php
/**
 * Magento Extension by TRIC Solutions
 *
 * @copyright  Copyright (c) 2012 TRIC Solutions (http://www.tric.dk)
 * @license    http://www.tric.dk/TRIC-LICENSE-COMMUNITY.txt
 * @store       http://store.tric.dk
 */
class TRIC_GLS_Block_Adminhtml_System_Config_Form_Field_Handlerates extends Mage_Adminhtml_Block_System_Config_Form_Field
{

	protected function _getElementHtml(Varien_Data_Form_Element_Abstract $element)
	{
		$this->setElement($element);

		$buttonBlock = $this->getLayout()->createBlock('adminhtml/widget_button');

		$params = array();
		if($buttonBlock->getRequest()->getParam('website'))
		{
			$params['website'] = $buttonBlock->getRequest()->getParam('website');
		}
		if($buttonBlock->getRequest()->getParam('store'))
		{
			$params['store'] = $buttonBlock->getRequest()->getParam('store');
		}

		$data = array(
			'label'     => Mage::helper('gls')->__('Håndter Satser'),
			'onclick'   => 'setLocation(\''.Mage::helper('adminhtml')->getUrl("adminhtml/adminhtml_gls_rates/", $params) . '\')',
			'class'     => '',
		);

		$html = $buttonBlock->setData($data)->toHtml();

		return $html;
	}


}
