<?php
/**
 * Magento Extension by TRIC Solutions
 *
 * @copyright  Copyright (c) 2012 TRIC Solutions (http://www.tric.dk)
 * @license    http://www.tric.dk/TRIC-LICENSE-COMMUNITY.txt
 * @store       http://store.tric.dk
 */

class TRIC_GLS_Block_Adminhtml_Website_Switcher extends Mage_Adminhtml_Block_Template
{

    protected $_website = null;
    protected $_websiteVarName = 'website';
    protected $_hasDefaultOption = false;

    public function __construct()
    {
        $helper = $this->_getHelper();
        parent::__construct();
        $this->setTemplate('gls/website/switcher.phtml');
        $this->setUseConfirm(true);
        $this->setUseAjax(true);
        $this->setDefaultWebsiteName($helper->__('All Websites'));
    }

    protected function _getHelper()
    {
        return Mage::helper('gls/rates');
    }

    public function getWebsites()
    {
        return $this->_getHelper()->getWebsites();
    }

    protected function getWebsite()
    {
        if(is_null($this->_website))
        {
	    	$this->_website = $this->_getHelper()->getWebsite();  
        } 
        return $this->_website;
    }
    
    public function getWebsiteId()
    {
        return $this->_getHelper()->getWebsiteId($this->getWebsite());
    }
    
    public function hasDefaultOption($hasDefaultOption = null)
    {
        if (null !== $hasDefaultOption)
        {
	        $this->_hasDefaultOption = $hasDefaultOption;
        }
        return $this->_hasDefaultOption;
    }

    public function getSwitchUrl()
    {
        if ($url = $this->getData('switch_url'))
        {
	        return $url;
        }
        return $this->getUrl('*/*/*', array('_current' => true, $this->_websiteVarName => null));
    }
}