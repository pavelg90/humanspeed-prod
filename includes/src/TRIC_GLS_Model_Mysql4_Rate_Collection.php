<?php
/**
 * Magento Extension by TRIC Solutions
 *
 * @copyright  Copyright (c) 2012 TRIC Solutions (http://www.tric.dk)
 * @license    http://www.tric.dk/TRIC-LICENSE-COMMUNITY.txt
 * @store       http://store.tric.dk
 */

class TRIC_GLS_Model_Mysql4_Rate_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract
{
    protected $_countryTable;
    protected $_regionTable;

    protected function _construct() 
    {
    	parent::_construct();
        $this->_init('gls/rate');
        $this->_countryTable    = $this->getTable('directory/country');
        $this->_regionTable     = $this->getTable('directory/country_region');
    }

    public function _initSelect()
    {
        parent::_initSelect();
        $this->_select->joinLeft(
                array('country_table' => $this->_countryTable), 
                'country_table.country_id = main_table.dest_country_id', 
                array('dest_country' => 'iso2_code'))
            ->joinLeft(
                array('region_table' => $this->_regionTable),
                'region_table.region_id = main_table.dest_region_id',
                array('dest_region' => 'code'));
    }

    public function setWebsiteFilter($websiteId)
    {
        return $this->addFieldToFilter('website_id', $websiteId);
    }
}