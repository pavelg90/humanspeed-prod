<?php
class TRIC_GLS_Model_Adminhtml_System_Config_Source_Icontype 
{
	public function toOptionArray() 
	{
		$icontypes = array();
		
		$icontypes[] = array(
						'value' => 'logo-only-heading',
						'label' => Mage::helper('gls')->__('Ét logo i stedet for overskrift')
					);

		$icontypes[] = array(
						'value' => 'logo-heading',
						'label' => Mage::helper('gls')->__('Ét logo sammen med overskrift')
					);
						
		$icontypes[] = array(
						'value' => 'bullets',
						'label' => Mage::helper('gls')->__('Et logo ud for hver GLS fragtsats')
					);	
		
		return $icontypes;
	}
}