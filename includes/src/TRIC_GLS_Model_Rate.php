<?php
/**
 * Magento Extension by TRIC Solutions
 *
 * @copyright  Copyright (c) 2012 TRIC Solutions (http://www.tric.dk)
 * @license    http://www.tric.dk/TRIC-LICENSE-COMMUNITY.txt
 * @store       http://store.tric.dk
 */

class TRIC_GLS_Model_Rate extends Mage_Core_Model_Abstract
{
	protected $_eventPrefix = 'gls_rate';
	protected $_eventObject = 'rates';
	protected $_cacheTag = 'gls_rate';

	protected function _construct()
	{
		parent::_construct();
		$this->_init('gls/rate');
	}

	protected function getHelper()
	{
		return Mage::helper('gls/rates');
	}

	protected function getFilters()
	{
		return array(
			'dest_country_id'     => $this->getDestCountryFilter(),
			'dest_region_id'      => $this->getDestRegionFilter(),
			'dest_zip'            => $this->getDestZipFilter(),
			'condition_name'      => $this->getConditionNameFilter(),
			'condition_from_value'=> $this->getConditionValueFilter(),
			'condition_to_value'  => $this->getConditionValueFilter(),
			'price'               => $this->getPriceFilter(),
			'cost'                => $this->getCostFilter(),
		);
	}

	protected function getValidators()
	{
		return array(
//			'dest_country_id'     => $this->getTextValidator(false, 0, 4),
			'dest_region_id'      => $this->getIntegerValidator(false, 0),
			'dest_zip'            => $this->getTextValidator(false, 0, 10),
			'condition_name'      => $this->getTextValidator(true, 0, 20),
			'condition_from_value'=> $this->getFloatValidator(false, 0),
			'condition_to_value'  => $this->getFloatValidator(false, 0),
			'price'               => $this->getFloatValidator(false, 0),
			'cost'                => $this->getFloatValidator(false, 0),
			'external_id'         => $this->getTextValidator(false, 0, 512),
		);
	}

	public function validate()
	{
		$helper = $this->getHelper();
		parent::validate();
		$errorMessages = array();
		$tablerate = Mage::getModel('gls/rate')->loadByRequest($this);
		if ($tablerate->getId()) {
			array_push($errorMessages, $helper->__('Duplicate rate.'));
		}
		if (count($errorMessages)) Mage::throwException(join("\n", $errorMessages));
		return true;
	}

	public function getTitle()
	{
		$helper = $this->getHelper();
		$title = null;
		$country = $region = null;
		if ($this->getDestRegionId()) $region = $helper->getRegion($this->getDestRegionId());
		if ($this->getDestCountryId()) $country = $helper->getCountry($this->getDestCountryId());
		$zip = $this->getDestZip();
		$conditionNames = Mage::getSingleton('shipping/carrier_tablerate')->getCode('condition_name');
		$conditionName = $this->getConditionName();
		$conditionName = (isset($conditionNames[$conditionName])) ? $conditionNames[$conditionName] : '';
		$conditionFromValue = $this->getConditionFromValue();
		$conditionToValue = $this->getConditionToValue();
		$title = ' '.implode(', ', array(
				(($country) ? $country->getName() : '*'),
				(($region) ? $region->getName() : '*'),
				(($zip) ? $zip : '*'),
				(($conditionName) ? $conditionName : ''),
				(($conditionFromValue) ? floatval($conditionFromValue) : '0'),
				(($conditionToValue) ? floatval($conditionToValue) : '0'),
			)).' ';
		return $title;
	}

	public function filterDestCountry($value)
	{
		$helper = $this->getHelper();
		if ($value && ($value != '*')) {
			$countriesISO2Codes = $helper->getCountriesISO2Codes();
			$countriesISO3Codes = $helper->getCountriesISO3Codes();
			if (isset($countriesISO2Codes[$value])) {
				$value = $value;
			} else if (in_array($value, $countriesISO2Codes)) {
					$value = array_search($value, $countriesISO2Codes);
				} elseif (in_array($value, $countriesISO3Codes)) {
				$value = array_search($value, $countriesISO3Codes);
			} else $value = '0';
		} else $value = '0';
		return $value;
	}

	public function filterDestRegion($value)
	{
		$helper = $this->getHelper();
		$countryId = $this->filterDestCountry($this->getData('dest_country_id'));
		if ($countryId && $value && ($value != '*')) {
			$regionsCodes = $helper->getRegionsCodes($countryId);
			if (is_array($regionsCodes)) {
				if (isset($regionsCodes[$value])) {
					$value = $value;
				} else if (in_array($value, $regionsCodes)) {
						$value = array_search($value, $regionsCodes);
					} else $value = '0';
			} else $value = '0';
		} else $value = '0';
		return $value;
	}

	public function filterDestZip($value)
	{
		return ($value == '' || $value == '*') ? '' : $value;
	}

	public function filterConditionName($value)
	{
		$values = Mage::getSingleton('shipping/carrier_tablerate')->getCode('condition_name');
		return (isset($values[$value])) ? $value : null;
	}

	protected function getDestCountryFilter()
	{
		return $this->getTextFilter()->appendFilter(new Zend_Filter_Callback(array(
					'callback' => array($this, 'filterDestCountry'),
				)));
	}

	protected function getDestRegionFilter()
	{
		return $this->getTextFilter()->appendFilter(new Zend_Filter_Callback(array(
					'callback' => array($this, 'filterDestRegion'),
				)));
	}

	protected function getDestZipFilter()
	{
		return $this->getTextFilter()->appendFilter(new Zend_Filter_Callback(array(
					'callback' => array($this, 'filterDestZip'),
				)));
	}

	protected function getConditionNameFilter()
	{
		return $this->getTextFilter()->appendFilter(new Zend_Filter_Callback(array(
					'callback' => array($this, 'filterConditionName'),
				)));
	}

	protected function getConditionValueFilter()
	{
		return $this->getTextFilter()->appendFilter(new Zend_Filter_Callback(array(
					'callback' => array($this, 'filterFloat'),
				)));
	}

	protected function getPriceFilter()
	{
		return $this->getTextFilter()->appendFilter(new Zend_Filter_Callback(array(
					'callback' => array($this, 'filterFloat'),
				)));
	}

	protected function getCostFilter()
	{
		return $this->getTextFilter()->appendFilter(new Zend_Filter_Callback(array(
					'callback' => array($this, 'filterFloat'),
				)));
	}

	public function loadByRequest(Varien_Object $request)
	{
		$this->_getResource()->loadByRequest($this, $request);
		$this->setOrigData();
		return $this;
	}


}