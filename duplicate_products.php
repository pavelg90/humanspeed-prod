<?php
  require_once 'app/Mage.php';
  Mage::app();
  
  $_storeEuId = 3;

  echo Mage::app()->getStore($_storeEuId)->getName(), PHP_EOL;

  $_productsSkus = array(
    '2017501',
    '2017503',
    '2017505',
    '2017507',
    '2017509',
    '2017511',
    '2017513',
  );
  
  $_productSizes = array(
    'XS',
    'S',
    'M',
    'L',
    'XL',
    'XXL',
    'XXXL'
  );
  $_XS = 13;  
  $_XXXL = 19;
  
  foreach ($_productsSkus as &$_sku) {
    echo '---Creating ', $_sku , PHP_EOL; 
    foreach ($_productSizes as &$_size) {
         
      if ($_size == 'XS' || $_size == 'XXXL') {
        $_product =
          Mage::getModel('catalog/product')
           ->loadByAttribute('sku', $_sku . '-L');
      } else {
        $_product =
          Mage::getModel('catalog/product')
           ->loadByAttribute('sku', $_sku . '-' . $_size);
      }
      
      $_newSku = 'custom-' . $_sku . '-' . $_size;
      $_newProduct =
            Mage::getModel('catalog/product')
              ->loadByAttribute('sku', $_newSku);

      if ($_newProduct) {

        echo '------' . $_newSku, ' already exists', PHP_EOL; 

      } elseif (!$_newProduct && $_product) {

        $_price = array(
          '2017501' => 100,
          '2017503' => 145,
          '2017505' => 200,
          '2017507' => 170,
          '2017509' => 92,
          '2017511' => 92,
          '2017513' => 195
        )[$_sku];
        
        
        echo '------Creating', $_sku, '-', $_size, PHP_EOL;
        $_newProduct = $_product->duplicate();
        $_newProduct = $_newProduct->load( $_newProduct->getId() );
        $_name = 'CUSTOM ' . $_product->getName();
        
        if ($_size == 'XS') {
          $_newProduct->setSize(13);
          $_name = str_replace('-L', '-XS', $_name); 
        } elseif ($_size == 'XXXL') {
          $_newProduct->setSize(19);
          $_name = str_replace('-L', '-XXXL', $_name); 
        }
                
        $_newProduct
          ->setSku($_newSku)
          ->setName($_name)
          ->setMetaTitle($_name)
          ->setStatus(1)
          ->setVisibility(Mage_Catalog_Model_Product_Visibility::VISIBILITY_NOT_VISIBLE)
          ->setStockData(array('qty' => 999, 'is_in_stock' => 1))
          ->setStoreId($_storeEuId)
          ->setPrice($_price)
          ->save();

        unset($_newProduct);
        unset($product);
      }
    };
  };