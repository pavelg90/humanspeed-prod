<?php
/**
 * Magento Extension by TRIC Solutions
 *
 * @copyright  Copyright (c) 2011 TRIC Solutions (http://www.tric.dk)
 * @license    http://www.tric.dk/TRIC-LICENSE-COMMUNITY.txt
 * @store       http://store.tric.dk
 */
class TRIC_GLS_Model_Carrier_Gls extends Mage_Shipping_Model_Carrier_Abstract
{
	const FREE_SHIPPING_RATES = 'gls_freeshipping_rates';
	const FREE_SHIPPING_RATES_FIELD = 'gls_specific_shipping_method';
	
	protected $_code = 'gls';
	public function isTrackingAvailable()
	{
		return true;
	}

	public function collectRates(Mage_Shipping_Model_Rate_Request $request)
	{
		if (!$this->getConfigFlag('active')) {
			return false;
		}

		$freeBoxes = 0;
		if ($request->getAllItems()) {
			foreach ($request->getAllItems() as $item) {

				if ($item->getProduct()->isVirtual() || $item->getParentItem()) {
					continue;
				}

				if ($item->getHasChildren() && $item->isShipSeparately()) {
					foreach ($item->getChildren() as $child) {
						if ($child->getFreeShipping() && !$child->getProduct()->isVirtual()) {
							$freeBoxes += $item->getQty() * $child->getQty();
						}
					}
				} elseif ($item->getFreeShipping()) {
					$freeBoxes += $item->getQty();
				}
			}
		}
		$this->setFreeBoxes($freeBoxes);

		// Free shipping by qty
		$freeQty = 0;
		if ($request->getAllItems()) {
			foreach ($request->getAllItems() as $item) {
				if ($item->getProduct()->isVirtual() || $item->getParentItem()) {
					continue;
				}

				if ($item->getHasChildren() && $item->isShipSeparately()) {
					foreach ($item->getChildren() as $child) {
						if ($child->getFreeShipping() && !$child->getProduct()->isVirtual()) {
							$freeQty += $item->getQty() * ($child->getQty() - (is_numeric($child->getFreeShipping()) ? $child->getFreeShipping() : 0));
						}
					}
				} elseif ($item->getFreeShipping()) {
					$freeQty += ($item->getQty() - (is_numeric($item->getFreeShipping()) ? $item->getFreeShipping() : 0));
				}
			}
		}

		if (!$request->getConditionName() || $request->getConditionName() != $this->getConfigData('condition_name')) {
			$request->setConditionName($this->getConfigData('condition_name') ? $this->getConfigData('condition_name') : $this->_default_condition_name);
		}
		
		$oldWeight = $request->getPackageWeight();
		$oldQty = $request->getPackageQty();

		$result = Mage::getModel('shipping/rate_result');

		$rates = $this->getRate($request);

		$request->setPackageWeight($oldWeight);
		$request->setPackageQty($oldQty);

		$lastMethodName = null;

		foreach($rates as $rate)
		{
			if (!empty($rate) && $rate['price'] >= 0)
			{
				$lastMethodName = $rate['method_name'];

				$method = Mage::getModel('shipping/rate_result_method');

				$method->setCarrier('gls');
				$method->setCarrierTitle($this->getConfigData('title'));

				$method->setMethod('bestway_' . $rate['pk']);
				$method->setMethodTitle($rate['method_name']);

				if ($request->getFreeShipping() === true || $request->getPackageQty() == $this->getFreeBoxes()) {
					$price = '0.00';
				} else {
					$price = $rate['price'];
				}

				if ($rate['condition_type'] == 'percent')
				{
					$price = ($price * $request->getData($request->getConditionName())) / 100;
				}

				$method->setPrice($this->getFinalPriceWithHandlingFee($price));
				$method->setCost($rate['cost']);
				
				$method->setPackageWeight($oldWeight);
				$method->setPackageQty($oldQty);
				
				$result->append($method);
			}
		}
		
		Mage::dispatchEvent('gls_rates_result_handle_specific_freeshipping', array(
            'quote'   => Mage::getSingleton('checkout/cart')->getQuote(),
            'result'  => $result,
        ));
		
		return $result;
	}

	public function getRate(Mage_Shipping_Model_Rate_Request $request)
	{
		return Mage::getResourceModel('gls_shipping/rate')->getNewRate($request);
	}

	public function getCode($type, $code='')
	{
		$codes = array(

			'condition_name'=>array(
				'package_weight' => Mage::helper('shipping')->__('Weight vs. Destination'),
				'package_value'  => Mage::helper('shipping')->__('Price vs. Destination'),
				'package_qty'    => Mage::helper('shipping')->__('# of Items vs. Destination'),
			),

			'condition_name_short'=>array(
				'package_weight' => Mage::helper('shipping')->__('Weight (and above)'),
				'package_value'  => Mage::helper('shipping')->__('Order Subtotal (and above)'),
				'package_qty'    => Mage::helper('shipping')->__('# of Items (and above)'),
			),

		);

		if (!isset($codes[$type])) {
			throw Mage::exception('Mage_Shipping', Mage::helper('shipping')->__('Invalid Tablerate Rate code type: %s', $type));
		}

		if (''===$code) {
			return $codes[$type];
		}

		if (!isset($codes[$type][$code])) {
			throw Mage::exception('Mage_Shipping', Mage::helper('shipping')->__('Invalid Tablerate Rate code for type %s: %s', $type, $code));
		}

		return $codes[$type][$code];
	}

	/**
	 * Get allowed shipping methods
	 *
	 * @return array
	 */
	
	public function getAllowedMethods()
	{
		$method_code = "bestway";
		$ratesCollection = Mage::getModel('gls/rate')->getCollection();
		$ratesCollection->addFieldToFilter('active',1);

		$website_names = array();
		$websites = Mage::app()->getWebsites();
		foreach($websites as $website) {
			$website_names[$website->getId()] = $website->getName();
		}

		$allowed_rates = array();
		foreach($ratesCollection as $rate) {
			if(!isset($website_names[$rate->getData('website_id')])) {
				continue;
			} 
			$title = $rate->getData('method_name') . " | ".trim($rate->getData('dest_country_id'),",")." - ".$rate->getData('condition_from_value')." - ".$rate->getData('condition_to_value');
			if(count($website_names) > 1) {
				$title .= " | ".$website_names[$rate->getData('website_id')];
			}
			$allowed_rates[$method_code."_".$rate->getData('pk')] = $title;
		}
		
		return $allowed_rates;
		
	}
	
	
	public function getExtraJsSalesRuleScript()
	{
		$field = self::FREE_SHIPPING_RATES;
		$script = "if('$field' == $('rule_simple_action').value) {
			$('rule_discount_qty').up().up().hide();
			$('rule_discount_step').up().up().hide();
			$('rule_apply_to_shipping').up().up().hide();
			$('rule_simple_free_shipping').up().up().hide();
		}";

		return $script;
	}
	
}