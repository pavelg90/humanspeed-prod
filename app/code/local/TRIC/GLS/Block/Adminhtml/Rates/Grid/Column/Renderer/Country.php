<?php
/**
 * Magento Extension by TRIC Solutions
 *
 * @copyright  Copyright (c) 2012 TRIC Solutions (http://www.tric.dk)
 * @license    http://www.tric.dk/TRIC-LICENSE-COMMUNITY.txt
 * @store       http://store.tric.dk
 */

class TRIC_GLS_Block_Adminhtml_Rates_Grid_Column_Renderer_Country extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Text
{
    public function _getValue(Varien_Object $row)
    {
        $value = parent::_getValue($row);
        if ($value === '0') { 
        	return '*';
        } 
        
    	$output = "";
    	$countries = explode(",",$value);
    	foreach($countries as $country) {
    		if($country) {
	        	$output .= Mage::app()->getLocale()->getCountryTranslation($country)."<br/>";
	        }
    	}
        
        return $output;
    }
}