<?php
/**
 * Magento Extension by TRIC Solutions
 *
 * @copyright  Copyright (c) 2012 TRIC Solutions (http://www.tric.dk)
 * @license    http://www.tric.dk/TRIC-LICENSE-COMMUNITY.txt
 * @store       http://store.tric.dk
 */

class TRIC_GLS_Block_Adminhtml_Rates_ExportGrid extends Mage_Adminhtml_Block_Widget_Grid
{
	protected $_websiteId;
	protected $_conditionName;

	public function __construct()
	{
		parent::__construct();
		$this->setId('shippingGlsExportGrid');
		$this->_exportPageSize = 10000;
	}

	public function setWebsiteId($websiteId)
	{
		$this->_websiteId = Mage::app()->getWebsite($websiteId)->getId();
		return $this;
	}

	public function getWebsiteId()
	{
		if (is_null($this->_websiteId)) {
			$this->_websiteId = Mage::app()->getWebsite()->getId();
		}
		return $this->_websiteId;
	}

	public function setConditionName($name)
	{
		$this->_conditionName = $name;
		return $this;
	}

	public function getConditionName()
	{
		return $this->_conditionName;
	}

	protected function _getHelper()
	{
		return Mage::helper('gls/rates');
	}

	protected function _getStore()
	{
		$storeId = (int) $this->getRequest()->getParam('store', 0);
		return Mage::app()->getStore($storeId);
	}

	protected function _prepareColumns()
	{
		parent::_prepareColumns();
		$helper = $this->_getHelper();
		$this->addColumn('dest_country_id', array(
				'header'    => $helper->__('Country'),
				'index'     => 'dest_country_id',
				'default'       => '*'
			));
		$this->addColumn('dest_region', array(
				'header'        => $helper->__('Region/State'),
				'index'         => 'dest_region',
				'default'       => '*'
			));
		$this->addColumn('dest_city', array(
				'header'        => $helper->__('City'),
				'index'         => 'dest_city',
				'default'       => '*'
			));
		$this->addColumn('dest_zip', array(
				'header'        => $helper->__('Zip/Postal Code From'),
				'index'         => 'dest_zip',
				'default'       => '*'
			));
		$this->addColumn('dest_zip_to', array(
				'header'        => $helper->__('Zip/Postal Code To'),
				'index'         => 'dest_zip_to',
				'default'       => '*'
			));
		$this->addColumn('condition_name', array(
				'header'        => $helper->__('Condition Name'),
				'index'         => 'condition_name',
				'default'       => 'package_value'
			));
		$this->addColumn('condition_from_value', array(
				'header'        => $helper->__('Condition Value From'),
				'index'         => 'condition_from_value'
			));
		$this->addColumn('condition_value_to', array(
				'header'        => $helper->__('Condition Value To'),
				'index'         => 'condition_to_value'
			));
		$this->addColumn('price', array(
				'header'        => $helper->__('Price'),
				'index'         => 'price'
			));
		$this->addColumn('cost', array(
				'header'        => $helper->__('Cost'),
				'index'         => 'cost'
			));
		$this->addColumn('method_code', array(
				'header'        => $helper->__('Shipping Type'),
				'index'         => 'method_code'
			));
		$this->addColumn('method_name', array(
				'header'        => $helper->__('Name'),
				'index'         => 'method_name'
			));
		$this->addColumn('method_description', array(
				'header'        => $helper->__('Description'),
				'index'         => 'method_description'
			));
		$this->addColumn('sort_order', array(
				'header'        => $helper->__('Sort Order'),
				'index'         => 'sort_order'
			));
		$this->addColumn('external_id', array(
				'header'        => $helper->__('External ID'),
				'index'         => 'external_id'
			));

		return $this;
	}

	protected function _prepareCollection()
	{
		$websiteId = $this->getWebsiteId();
		$collection = Mage::getModel('gls/rate')->getCollection();
		
		$select = $collection->getSelect();
		$select->where('active = 1');
		
		if ($websiteId) $select->where('website_id = ?', $websiteId);
		else $select->where('website_id = -1');
		
		$this->setCollection($collection);
		return Mage_Adminhtml_Block_Widget_Grid::_prepareCollection();
	}

}