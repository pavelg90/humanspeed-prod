<?php
/**
 * Magento Extension by TRIC Solutions
 *
 * @copyright  Copyright (c) 2011 TRIC Solutions (http://www.tric.dk)
 * @license    http://www.tric.dk/TRIC-LICENSE-COMMUNITY.txt
 * @store       http://store.tric.dk
 */

class TRIC_GLS_Block_Droppoints extends Mage_Checkout_Block_Onepage_Abstract
{
	private $__prefix = "gls-";
	public function getPostnr(){

		if(isset($_SESSION[$this->__prefix.'postnr'])){
			$this->postnr = $_SESSION[$this->__prefix.'postnr'];
		}
		elseif(is_null($this->postnr)){
			$this->postnr = ($this->getCheckout()->getQuote()->getShippingAddress()->getData('postcode') != "-") ? $this->getCheckout()->getQuote()->getShippingAddress()->getData('postcode') : "";
		}
		return $this->postnr;
	}
	
	public function getAfhenter(){

		if(isset($_SESSION[$this->__prefix.'afhenter'])){
			$this->afhenter = $_SESSION[$this->__prefix.'afhenter'];
		}
		elseif(is_null($this->afhenter)){
			$firstname = $this->getCheckout()->getQuote()->getShippingAddress()->getData('firstname');
			$lastname = $this->getCheckout()->getQuote()->getShippingAddress()->getData('lastname');
			$this->afhenter = $firstname.' '.$lastname;
		}
		return str_replace('%20',' ',$this->afhenter);
	}
	
	public function getTelephone(){

		if(isset($_SESSION[$this->__prefix.'telephone'])){
			$this->telephone = $_SESSION[$this->__prefix.'telephone'];
		}
		elseif(is_null($this->telephone)){
			$telephone = $this->getCheckout()->getQuote()->getBillingAddress()->getData('telephone');

			$this->telephone = $telephone;
		}
		$this->telephone = ($this->telephone == '-') ? '' : $this->telephone;
		return str_replace('%20',' ',$this->telephone);
	}
}