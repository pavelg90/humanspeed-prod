<?php
/**
 * Magento Extension by TRIC Solutions
 *
 * @copyright  Copyright (c) 2011 TRIC Solutions (http://www.tric.dk)
 * @license    http://www.tric.dk/TRIC-LICENSE-COMMUNITY.txt
 * @store       http://store.tric.dk
 */


$installer = $this;
$installer->startSetup();

$table = $installer->getTable('shipping_gls');

try {
	$installer->run("ALTER TABLE $table DROP INDEX dest_country;");
} catch(Exception $e) {}

try {
	$resource = Mage::getSingleton('core/resource');
	$readConnection = $resource->getConnection('core_read');
	$query = "SELECT * FROM $table";
	$results = $readConnection->fetchAll($query);
	if(count($results) == 0) { 
	
		$installer->run("INSERT INTO {$installer->getTable('shipping_gls')} (`pk`, `website_id`, `dest_country_id`, `dest_region_id`, `dest_city`, `dest_zip`, `dest_zip_to`, `condition_name`, `condition_from_value`, `condition_to_value`, `condition_type`, `method_code`, `method_name`, `method_description`, `price`, `cost`, `sort_order`, `external_id`, `active`) VALUES
			(NULL, 1, 'DK', 0, '', '', '', 'package_weight', 0.0000, 30.0000, 'weight', 'gls_pakkeshop', 'GLS Pakkeshop', 'GLS Pakkeshop fragtmetode', 49.0000, 0.0000, 0, '', 1),
			(NULL, 1, 'DK', 0, '', '', '', 'package_weight', 0.0000, 99999999.0000, 'weight', 'gls_erhverv', 'GLS Erhverv', 'GLS Erhverv fragtmetode', 39.0000, 0.0000, 2, '', 1),
			(NULL, 1, '0', 0, '', '', '', 'package_weight', 0.0000, 99999999.0000, 'weight', 'gls_international', 'GLS International', 'GLS International fragtmetode', 99.0000, 0.0000, 0, '', 1),
			(NULL, 1, 'DK', 0, '', '', '', 'package_weight', 0.0000, 30.0000, 'value', 'gls_pakkeshop', 'GLS Pakkeshop', 'GLS Pakkeshop fragtmetode', 49.0000, 0.0000, 0, '', 1),
			(NULL, 1, 'DK', 0, '', '', '', 'package_weight', 0.0000, 99999999.0000, 'value', 'gls_express12', 'GLS Express - Før 12-levering', '', 89.0000, 0.0000, 4, '', 1),
			(NULL, 1, 'DK', 0, '', '', '', 'package_weight', 0.0000, 99999999.0000, 'value', 'gls_express10', 'GLS Express - Før 10-levering', '', 99.0000, 0.0000, 3, '', 1);
		");
	}
} catch(Exception $e) {}	

$installer->endSetup();