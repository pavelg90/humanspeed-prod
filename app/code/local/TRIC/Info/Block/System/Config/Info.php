<?php
/**
 * Magento Extension by TRIC Solutions
 *
 * @copyright  Copyright (c) 2015 TRIC Solutions (http://www.tric.dk)
 * @license    http://www.tric.dk/TRIC-LICENSE-COMMUNITY.txt
 * @store       http://store.tric.dk
 */


class TRIC_Info_Block_System_Config_Info extends Mage_Adminhtml_Block_Abstract implements Varien_Data_Form_Element_Renderer_Interface
{
	public function render(Varien_Data_Form_Element_Abstract $element)
	{
		$html = '';

		try {
			if(Mage::app()->getLocale()->getLocaleCode() == 'da_DK') {
				$url = 'http://info.services.tric.dk/about_dk.html';
			} else {
				$url = 'http://info.services.tric.dk/about_en.html';
			}

			if (!(Mage::app()->loadCache('tric_info_updates_feed_lastcheck')) || (time() - Mage::app()->loadCache('tric_info_updates_feed_lastcheck')) > 12*60*60) {
				Mage::getModel('tric_info/feed_updates')->check();
			}

			if (!(Mage::app()->loadCache('tric_info_block')) || (time() - Mage::app()->loadCache('tric_info_block_lastcheck')) > 12*60*60) {
				if(extension_loaded('curl')) {

					$ch = curl_init($url);
					curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
					curl_setopt($ch, CURLOPT_NOSIGNAL, 1);
					curl_setopt($ch, CURLOPT_TIMEOUT, 1);
					curl_setopt($ch, CURLOPT_TIMEOUT_MS, 200);

					$html = curl_exec($ch);

					$httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
					$errorNo = curl_errno($ch);

					curl_close($ch);

					if($httpCode != 200 || $errorNo > 0) {
						return '';
					}
				}

				if(!$html){
					return '';
				}

				Mage::app()->saveCache(serialize($html), 'tric_info_block');
				Mage::app()->saveCache(time(), 'tric_info_block_lastcheck');
				return $html;
			}
			$html = unserialize(Mage::app()->loadCache('tric_info_block'));
			return $html;
		}
		catch (Exception $e) {
			return '';
		}
	}
}
