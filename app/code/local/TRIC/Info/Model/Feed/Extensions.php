<?php
/**
* Magento Extension by TRIC Solutions
*
* @copyright  Copyright (c) 2015 TRIC Solutions (http://www.tric.dk)
* @license    http://www.tric.dk/TRIC-LICENSE-COMMUNITY.txt
* @store       http://store.tric.dk
*/

class TRIC_Info_Model_Feed_Extensions extends TRIC_Info_Model_Feed_Abstract
{
    public function getFeedUrl()
    {
        return TRIC_Info_Helper_Config::EXTENSIONS_FEED_URL;
    }
    
    public function check()
    {
        $extensions = array();
        
        try 
        {
	        if((time() - Mage::app()->loadCache('tric_info_extensions_feed_lastcheck')) < 3600){
		        return true;
	        }
            $node = $this->getFeedData();
            
            if(!$node)
            {
				return false;
            }
            foreach ($node->children() as $extension) 
            {
                $extensions[trim((string)$extension->name)] = array(
                    'display_name' => trim((string)$extension->display_name),
                    'display_name_dk' => trim((string)$extension->display_name_dk),
                    'version' => trim((string)$extension->version),
                    'url' => trim((string)$extension->url),
                    'url_dk' => trim((string)$extension->url_dk),
                    'guide' => trim((string)$extension->guide)
                );
            }
            
            Mage::app()->saveCache(serialize($extensions), 'tric_info_extensions_feed');
            Mage::app()->saveCache(time(), 'tric_info_extensions_feed_lastcheck');
            return true;
        } 
        catch (Exception $e) 
        {	
            return false;
        }
    }
    
}