<?php
/**
 * Magento Extension by TRIC Solutions
 *
 * @copyright  Copyright (c) 2015 TRIC Solutions (http://www.tric.dk)
 * @license    http://www.tric.dk/TRIC-LICENSE-COMMUNITY.txt
 * @store       http://store.tric.dk
 */

class TRIC_Info_Model_Feed_Updates extends TRIC_Info_Model_Feed_Abstract
{
	public function getFeedUrl()
	{
		$feedUrl = TRIC_Info_Helper_Config::UPDATES_FEED_URL;

		if(Mage::app()->getLocale()->getLocaleCode() == 'da_DK'){
			$feedUrl = str_replace('updates.xml', 'updates_dk.xml', $feedUrl);
		}
		else{
			$feedUrl = str_replace('updates.xml', 'updates_en.xml', $feedUrl);
		}
		return $feedUrl;
	}

	public function check()
	{
		$feedData = array();

		try {
			if((time() - Mage::app()->loadCache('tric_info_updates_feed_lastcheck')) < 3600){
		        return true;
	        }
	        
	        if(!$this->getInterests()){
		        return false;
	        }
	        
			if(!$node = $this->getFeedData()){
				return false;
			}

			foreach ($node->children() as $item) {
				
				$date = strtotime(trim((string)$item->date));
				if ($this->isInteresting($item,$date)) {

					if (!Mage::getStoreConfig('tric_info/install/date') || (Mage::getStoreConfig('tric_info/install/date') < $date)) {
						$feedData[] = array(
							'severity' => trim((string)$item->severity),
							'date_added' => $this->getDate(trim((string)$item->date)),
							'title' => trim((string)$item->title),
							'description' => trim((string)$item->content),
							'url' => trim((string)$item->url),
						);
					}
				}
			}

			$adminnotificationModel = Mage::getModel('adminnotification/inbox');
			if ($feedData && is_object($adminnotificationModel)) {
				$adminnotificationModel->parse(($feedData));
			}

			Mage::app()->saveCache(time(), 'tric_info_updates_feed_lastcheck');
			return true;
		} catch (Exception $e) {
			return false;
		}
	}

	public function getInterests()
	{
		if (!$this->getData('interests')) {
			$types = explode(',', Mage::getStoreConfig('tric_info/feed/interests'));
			$this->setData('interests', $types);
		}
		
		return $this->getData('interests');
	}

	public function isInteresting($item,$date)
	{
		$interests = $this->getInterests();

		$types = explode(",", trim((string)$item->type));
		$exts = explode(",", trim((string)$item->extensions));

		foreach ($types as $type) {
			if (array_search($type, $interests) !== false) {
				return true;
			}
			if ($type == TRIC_Info_Model_Source_Updates_Type::TYPE_UPDATE_RELEASE) {
				foreach ($exts as $ext) {
					if ($this->isExtensionInstalled($ext)) {
						$ext = strtolower($ext);
						if (!Mage::getStoreConfig($ext.'/install/date') || (Mage::getStoreConfig($ext.'/install/date') < $date)) {
							return true;
						}
					}
				}
			}
		}
		return false;
	}

	public function isExtensionInstalled($code)
	{
		$modules = array_keys((array)Mage::getConfig()->getNode('modules')->children());

		foreach ($modules as $moduleName) {
			if ($moduleName == $code) {
				return true;
			}
		}
		return false;
	}

}