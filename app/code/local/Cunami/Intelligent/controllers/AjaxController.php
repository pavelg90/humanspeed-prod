<?php

class Cunami_Intelligent_AjaxController extends Mage_Core_Controller_Front_Action
{
    public function indexAction()
    {
    }

    public function initsessionAction()
    {
      $session = $this->getRequest()->getParam('sessionId');
      $session_directory = Mage::getBaseDir().DS.'public'.DS.'data'.DS.$session;

      foreach (array('/', '/icons', '/designs') as $value) {
        mkdir($session_directory.$value, 0777, true);
      }

      foreach (array('/front', '/side', '/back') as $value) {
        file_put_contents($session_directory.$value.'.svg', '');
        chmod($session_directory.$value.'.svg', 0700);
      }
    }

    public function generateidAction()
    {
      echo md5(time());
      exit;
    }

    public function getextradesignsAction()
    {
      $session = $this->getRequest()->getParam('session');
      $product = $this->getRequest()->getParam('product');

      $files = scandir(Mage::getBaseDir().DS.'public'.DS.'data'.DS.$session .DS.'designs'.DS.$product.DS);

      print_r($files[count($files) - 1]);
      exit;
    }

    public function saveAction()
    {
      $session = $this->getRequest()->getParam('sessionId');
      $svg = $this->getRequest()->getParam('svg');
      $view = $this->getRequest()->getParam('view');
      $fileName = Mage::getBaseDir().DS.'public'.DS.'data'.DS.(string)$session.DS.$view.".svg";
      file_put_contents($fileName, $svg);
      chmod($fileName, 0700);
    }
  
    public function frontbysessionAction()
    {
      $session = $this->getRequest()->getParam('session');
      echo readfile(Mage::getBaseDir().DS.'public'.DS.'data'.DS.$session.DS.'front.svg');
      exit;
    }

    public function saveiconAction()
    {
      $session = $this->getRequest()->getParam('session');
      $storeFolder = Mage::getBaseDir().DS.'public'.DS.'data'.DS.$session.DS.'icons';

      if (!empty($_FILES)) {
        $tempFile = $_FILES['file']['tmp_name'];
        $targetFile =  $storeFolder.DS.$_FILES['file']['name'];
        move_uploaded_file($tempFile, $targetFile);
        chmod($targetFile, 0700);
      }
    }
    public function savedesignAction()
    {
      $session = $this->getRequest()->getParam('sessionId');
      $session_directory = Mage::getBaseDir().DS.'public'.DS.'data'.DS.$session;
      
      $product = $this->getRequest()->getParam('productId');
      $number = $this->getRequest()->getParam('number');
      $design_dir_name = $session_directory.$value.DS.'designs'.DS.$product;
      if (!file_exists($design_dir_name) && !is_dir($design_dir_name)) {
        mkdir($design_dir_name, 0777, true);
      }
      mkdir($design_dir_name.DS.$number, 0777, true);

      foreach (array('/front', '/side', '/back') as $value) {
        $fileName = $design_dir_name.DS.$number.DS.$value.'.svg';
        file_put_contents($fileName, file_get_contents($session_directory.DS.$value.'.svg'));
        chmod($fileName, 0700);
      }
    }
  
    public function updatedesignAction() {
      $session = $this->getRequest()->getParam('sessionId');
      $session_directory = Mage::getBaseDir().DS.'public'.DS.'data'.DS.$session;
      
      $product = $this->getRequest()->getParam('productId');
      $number = $this->getRequest()->getParam('number');
      $design_dir_name = $session_directory.$value.DS.'designs'.DS.$product;

      foreach (array('/front', '/side', '/back') as $value) {
        $fileName = $design_dir_name.DS.$number.DS.$value.'.svg';
        file_put_contents($fileName, file_get_contents($session_directory.DS.$value.'.svg'));
        chmod($fileName, 0700);
      }
    }

    public function getfilesAction()
    {
      $session = $this->getRequest()->getParam('session');
      $session_directory = Mage::getBaseDir().DS.'public'.DS.'data'.DS.$session;
      $product = $this->getRequest()->getParam('product');
      $design = $this->getRequest()->getParam('design');

      $dir = $session_directory.$value.DS.'designs'.DS.$product.DS.$design.DS;
      $files = scandir($dir);
      $result = '';
      for ($i = 0; $i < count($files); $i++) {
          if ($files[$i] != '.' && $files[$i] != '..') {
              if ($i + 1 != count($files)) {
                echo $files[$i] . ',';
              } else {
                echo $files[$i];
              }
          }
      }

      exit;
    }
  
    public function addproducttocartAction()
    {
      $_sku = 'custom-' . $this->getRequest()->getParam('sku');
      $_qty = $this->getRequest()->getParam('qty');
      $_liveLink = $this->getRequest()->getParam('liveLink');
        
      Mage::app();
      Mage::getSingleton('core/session', array('name'=>'frontend'));
      $_cart = Mage::getSingleton('checkout/cart');
      $_cart->init();
      
      try {
        $_productObj = Mage::getModel('catalog/product')->loadByAttribute('sku',$_sku);
        
        if (is_null($_productObj)) throw new Exception('Sku not found.');

        $_params = array(
          'product' => $_productObj->getId(),
          'qty' => $_qty
        );
        
        $_cart->addProduct($_productObj, $_params);    

      } catch (Exception $e) {    
//         echo $e->getMessage();    
      }

      $_cart->save();
      Mage::getSingleton('checkout/session')->setCartWasUpdated(true);
      
        $writeConnection = Mage::getSingleton('core/resource')->getConnection('core_write');
        $query = sprintf('INSERT INTO mg_live_links(live_link, cart_id, entity_id) VALUES("%s","%s","%s")'
                         , $_liveLink
                         , $_cart->getQuote()->getEntityId()
                         , $_productObj->getId());
 
//         echo $query;
        $writeConnection->query($query);

      exit;
    }
}
