<?php

class Cunami_Intelligent_BuilderController extends Mage_Core_Controller_Front_Action
{
    public function indexAction()
    {
      $locale = 'en_US';
      Mage::app()->getLocale()->setLocaleCode($locale);
      Mage::getSingleton('core/translate')->setLocale($locale)->init('frontend', true);
      
      $this->loadLayout();
      $this->renderLayout();
    }
}
