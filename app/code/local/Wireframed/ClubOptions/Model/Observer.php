<?php
 
class Wireframed_ClubOptions_Model_Observer {
    public function cmsField($observer) {
        //get CMS model with data
        $model = Mage::registry('cms_page');
        //get form instance
        $form = $observer->getForm();
        //create new custom fieldset 'atwix_content_fieldset'
        $fieldset = $form->addFieldset('wireframed_content_fieldset', array('legend'=>Mage::helper('cms')->__('Club Options'),'class'=>'fieldset-wide'));
        //add new field
        $fieldset->addField('content_custom', 'text', array(
            'name'      => 'club_name',
            'label'     => Mage::helper('cms')->__('Club Name'),
            'title'     => Mage::helper('cms')->__('Club Name'),
            'disabled'  => false,
            //set field value
            'value'     => $model->getContentCustom()
        ));
 
    }
}